# booktemplate

[LaTeX](https://www.latex-project.org/) template for compiling long documents. I made this template for my personal use. See the PDF result here [[download](https://gitlab.com/nithiya/booktemplate/-/jobs/artifacts/master/raw/docmain.pdf?job=pdf)].

## Packages

All required packages are available on [CTAN](https://www.ctan.org/). It is recommended to use a TeX distribution, such as [TeX Live](https://tug.org/texlive/), to ensure all requirements are satisfied. The [`book`](https://www.ctan.org/pkg/book) document class is used.

The PDF file is built using either [XeLaTeX](http://xetex.sourceforge.net/) or [LuaLaTeX](http://luatex.org/) with [glossaries](https://www.ctan.org/pkg/glossaries) and [Biber](http://biblatex-biber.sourceforge.net/) via [Arara](https://ctan.org/pkg/arara):

```sh
arara docmain.tex
```

The Arara directives used are as follows (replace `xelatex` with `lualatex`, if necessary):

```latex
% arara: xelatex: { shell: yes }
% arara: makeglossaries
% arara: biber
% arara: xelatex: { shell: yes }
% arara: xelatex: { shell: yes, synctex: yes }
```

Note that Arara requires [Java](https://jdk.java.net/). Additionally, the `minted` package requires [Python](https://www.python.org/) and the [Pygments](https://pygments.org/) library.

An alternative is to use the latest TeX Live Docker image by [Island of TeX](https://gitlab.com/islandoftex/images/texlive), which can also be used with GitLab CI. The following is a minimal example of a valid `.gitlab-ci.yml` configuration:

```yml
image: registry.gitlab.com/islandoftex/images/texlive:latest

build:
  script:
    - arara docmain.tex
  artifacts:
    paths:
      - ./*.pdf
```

The resulting PDF file will be available as an artifact once the build is complete. See this [TUGboat article](https://tug.org/TUGboat/tb40-3/tb126island-docker.pdf) for more information.

If you have [Docker](https://www.docker.com/) installed on your computer, you can use the [following command](https://gitlab.com/islandoftex/images/texlive/-/wikis/Building-LaTeX-documents-locally-using-Docker) to compile the PDF:

```sh
docker pull registry.gitlab.com/islandoftex/images/texlive:latest
docker run --interactive --rm --name latex --volume "$PWD":/usr/src/app --workdir /usr/src/app registry.gitlab.com/islandoftex/images/texlive:latest arara docmain.tex
```

## Credits

- inspired by [Edward R. Tufte](https://www.edwardtufte.com/tufte/)'s works
- examples may be derived from [TeX Stack Exchange](https://tex.stackexchange.com) (CC-BY-SA), [Wikibooks](https://en.wikibooks.org/wiki/LaTeX) (CC-BY-SA), CTAN documentation, [Dickimaw Books](https://www.dickimaw-books.com/latexresources.html), and [Overleaf](https://www.overleaf.com/learn)
  - a longtable example by [LianTze Lim on Overleaf](https://www.overleaf.com/latex/examples/a-longtable-example/xxwzfxkxxjmc) (CC-BY-4.0)
  - sample bibliography from [BibLaTeX examples on CTAN](http://mirrors.ctan.org/macros/latex/contrib/biblatex/doc/examples/biblatex-examples.bib)
  - some glossaries examples are taken from the [glossaries package on CTAN](https://www.ctan.org/pkg/glossaries)
- example images are from [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page)
  - Eurasian tree sparrow (*Passer montanus malaccensis*), adult male, in Kuala Lumpur, Malaysia; taken on 31 January 2019, 15:20:47 by [Peter P. Othagoer](https://commons.wikimedia.org/wiki/File:Passer_montanus_malaccensis_@_Kuala_Lumpur,_Malaysia_%281%29.jpg) (CC-BY-4.0)
- dummy text from [baconipsum.com](https://baconipsum.com/), [veggieipsum.com](https://veggieipsum.com/), and [tunaipsum.com](https://tunaipsum.com/)

## License

This work is licensed under the terms of the [LaTeX Project Public License, Version 1.3c (LPPL-1.3c)](https://www.latex-project.org/lppl/).
